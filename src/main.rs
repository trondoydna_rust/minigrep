extern crate minigrep;

use std::env;
use std::process;

use minigrep::Config;

fn main() {
    let config = Config::new( env::args()).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    match minigrep::run(config) {
        Ok(v) => v.into_iter()
            .for_each(|l| println!("{}", l)),
        Err(e) => {
            println!("Application error: {}", e);
            process::exit(1);
        }
    };
}

